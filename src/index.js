const napolitano = document.querySelector('#napolitano')
const txtNap = document.querySelector('#nap')
const napos = ['na', 'poli', 'tano']
let times = 0
let amount = 3

function generateNapolitano() {
    if (times === 10) {
        amount += 2
    } else if (times > 11 && times % 5 === 0) {
        amount += 2
    }

    let html = ''
    let description = ''

    for (let i = 0; i < amount; i++) {
        const nap = napos[parseInt(Math.random() * 3, 10)]
        html += `<li class="${nap}"></li>`
        description += nap
    }

    napolitano.innerHTML = html
    nap.innerHTML = description
    times++
}